Morar em São Paulo me fez rever minhas escolhas...
Comecei a visitar mais as feiras, procurar evitar os supermercados e percebi que a bicicleta me garantia muito mais autonomia que um carro ou o transporte público.

Numa dessas encontrei na feira perto do Minhocão um senhor que vendia tapioca, eita tapioca boa! Além dela, ele também vendia o côco que ele mesmo ralava para a tapioca. Comprei o côco e a tapioca, ele até embrulhou o côco numa folha de repolho para consevar melhor o bichinho e me avisou:
 - Guarda na geladeira minha filha.

Passa, que passando o tempo, passei na casa de um amigo, passou dia e o côco passou o tempo dentro da mochila e passou, resultado... Côco azedo... Comi, não perdi, mas ainda não era a mesma coisa.

Passou o tempo e acabei visitando essa feira de novo, visito o tapioqueiro e aproveito o combo: café da manhã com tapioca e côco para levar. Dessa vez eu guardo o côco!

Uma amiga de Porto Alegre estava me visitando, nessa época morava na Avenida 9 de Julho, de cara com o viaduto da Rua Major Quedinho, da janela da cozinha dava para ver a escadaria que liga o viaduto até a avenida. Acontece que aquele caminho, a escadaria, se tornava um tipo de caminho oculto, usado de banheiro, quarto, cozinha e "encurralador". Tem gente que toma, toma água, toma cachaça, toma crack e toma as coisas das outras pessoas e aquela escadaria é um lugar para se tomar todo tipo de coisa, tanto que quem descia por alí já tinha o hábito de verificar se havia algum possível tomador subindo por alí antes de descer.

Eu sou do time que toma água, água que passarinho bebe e não bebe... Um dia, ajudando numa festa lá do Bixiga, tomei as minhas águas e enquanto lavava uma louça tinha uma garrafa de cachaça alí do meu lado e pensei "já que tô ajudando, não passa nada tomar um cortinho dessa branquinha". Passou o tempo e chegou a hora de voltar para casa, minha amiga tava lá me esperando. Peguei a bicicleta, a mochila e segui meu caminho.

Normalmente procurava evitar a escadaria de noite, além do mais, de bicicleta era mais fácil fazer a volta pela rua. Acontece que depois de tomar umas, incluindo a pressa, decidi descer por alí mesmo e nem me liguei de verificar se tinha alguém subindo ou não. Começo a descer e me deparo com um casal, me param, avisam que não querem a bicicleta mas que era para passar as coisas.

Acho que nesse dia, tomei tudo menos a minha dose de bom senso, ponho a mochila para frente e começo a procurar o celular... O casal se entreolha e o cara me pergunta:
 - O que você tá fazendo?
 - Uai, tô procurando o celular.
 - Não, deixa disso e passa tudo!

Passei. Ela pega a mochila e eles seguem o caminho, subindo a escadaria. Sigo o meu, sigo descendo e logo abaixo, depois de fazer a curva da escadaria um outro cara me para e recomeça o mesmo diálogo:
 - "Não quero a bicicleta..."
Nesse instante ele pausa, olha para cima e percebe que o casal já havia levado a mochila e me deixa passar.
Termino de descer a escadaria, merda. Pelo menos ainda tenho a bicicleta.
Chego em casa, guardo a Fernanda, digo, a bicicleta na garagem e subo até o apartamento.

Abro a porta e minha amiga me recebe:
 - Como você tá? O que aconteceu? Eu acabei de te ver descendo a escadaria e você demorou para chegar.
 - Ahhh... acabaram de me roubar.
 - E a Fernanda!?
 - Tá bem, deve tá um pouco assustada, ela tá la embaixo.
 - Então o que levaram?

Coloco a mão em um bolso e retiro as chaves, outro bolso e retiro a carteira e o celular. Paro, penso... O côco!

Do mesmo jeito que eu tomei o que não me pertencia, a vida veio e tomou o que eu mais queria naquele momento. Espero que eles tenham aproveitado o côco mais do que eu.
